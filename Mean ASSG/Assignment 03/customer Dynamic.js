const express = require('express')

const db = require('../db')

const utils = require('../utils')
const { request, response } = require('express')

const router =  express.Router()

router.get('/',(request,response)=>{
    console.log('customer/get')
    const ID = request.body.ID

    const statement = `select * from Customer where ID = '${ID}'`
    db.query(statement,(error,data)=>{
        response.send(utils.createResult(error,data))
    })
})

router.post('/',(request,response)=>{
  
    const Name = request.body.Name
    const Password = request.body.Password
    const Mobile = request.body.Mobile
    const Address = request.body.Address
    const Email = request.body.Email

    const state = `insert into Customer (Name,Password,Mobile,Address,Email)
    values ('${Name}','${Password}','${Mobile}','${Address}','${Email}')`

    db.query(state,(err,data)=>{
        response.send(utils.createResult(err,data))
    })
})

router.delete('/',(request,response)=>{
    const ID = request.body.ID

    const state = `delete from Customer where ID = '${ID}'`

    db.query(state,(err,data)=>{
        response.send(utils.createResult(err,data))
    })

})

module.exports = router