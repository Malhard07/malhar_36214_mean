const express = require('express')


const bodyparser = require('body-parser')
const app = express()
app.use(bodyparser.json())

const routerPizza = require('./Pizza Dynamic')
app.use(routerPizza)

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
  })