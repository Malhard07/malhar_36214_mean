const express = require('express')
const app = express()
const db = require('./db')
const utilities = require('./utilities')
const router = express.Router()


// -----------------------------------  GET  ---------------------------------

router.get('/product', (request, response) => {

  const statement = `select * from PIZZA_ITEMS`
  db.query(statement, (error, dbResult) => {
    response.send(utilities.createResult(error, dbResult))
  })
})


// -----------------------------------  POST  ---------------------------------

router.post('/product',(request, response) => {


const statement = `INSERT INTO PIZZA_ITEMS
                  (Name, Type, Category, Description)
                  VALUES
                  ('${request.body.Name}',
                  '${request.body.Type}',
                  '${request.body.Category}',
                  '${request.body.Description}')`

db.query(statement, (error,dbResult) => {
  response.send(utilities.createResult(error, dbResult))
})                

 })
// -----------------------------------  PUT  ---------------------------------
router.put('/product', (request,response) => {



const statement = `UPDATE PIZZA_ITEMS
SET 
  Name = '${request.body.Name}',    
  Type = '${request.body.Type}',
  Category = '${request.body.Category}',
  Description = '${request.body.Description}'
WHERE ID= '${request.body.ID}'`
db.query(statement, (error,dbResult) => {
  response.send(utilities.createResult(error, dbResult))

})

})

// -----------------------------------  DELETE  ---------------------------------

router.delete('/product', (request,response) => {



const statement = `DELETE FROM PIZZA_ITEMS WHERE ID = ${request.body.ID}`
db.query(statement, (error,dbResult) => {
  response.send(utilities.createResult(error, dbResult))

})

})

module.exports = router


