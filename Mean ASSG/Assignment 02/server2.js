const express = require('express')
const app = express()
const mysql = require('mysql')
const bodyParser = require('body-parser')

app.use(bodyParser.json())

// -----------------------------------  GET  ---------------------------------

app.get('/product', (request, response) => {


  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database: 'dac_db'
  })

  
  connection.connect()

  
  const statement = `select * from PIZZA_ITEMS`

 
  connection.query(statement, (error, data) => {

    connection.end()

    
    if (error) {
      response.end('error while executing query')
    } else {

     
      response.send(data)

    }
  })

})

// -----------------------------------  POST  ---------------------------------

app.post('/product',(request, response) => {

const connection = mysql.createConnection({
  host:'127.0.0.1',
  user: 'root',
  password: 'manager',
  database: 'dac_db'
})

const statement = `INSERT INTO PIZZA_ITEMS
                  (Name, Type, Category, Description)
                  VALUES
                  ('${request.body.Name}',
                  '${request.body.Type}',
                  '${request.body.Category}',
                  '${request.body.Description}')`

                

connection.query(statement,(error,result) => {
connection.end()
response.send(result)
})
})
// -----------------------------------  PUT  ---------------------------------
app.put('/product', (request,response) => {

const connection = mysql.createConnection({
  host:'127.0.0.1',
  user:'root',
  password:'manager',
  database:'dac_db'
})

const statement = `UPDATE PIZZA_ITEMS
SET Description = '${request.body.Description}'
WHERE ID= '${request.body.ID}'`

connection.query(statement, (error,result) => {
  connection.end()
  response.send(result)
})

})

// -----------------------------------  DELETE  ---------------------------------

app.delete('/product', (request,response) => {

const connection = mysql.createConnection({
  host:'127.0.0.1',
  user:'root',
  password:'manager',
  database: 'dac_db'
})

const statement = `DELETE FROM PIZZA_ITEMS WHERE ID = ${request.body.ID}`

connection.query(statement, (error,result) => {
  connection.end()
  response.send(result)
})

})


app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})

