const express = require('express')
const router = express.Router()
const db = require('../db')
const utils = require('../utils')
const { response } = require('express')


router.get('/DistinctSubjects', (request, response) => {
    
    const statement = `select DISTINCT subject from books GROUP BY subject;`
    db.query(statement, (error, users) => {
        if(error)
        {
            response.send({'status':'error', 'error':error})
        }
        else{
            if(users.length == 0)
            {
                response.send({'status':'error', 'error':'user not found'})
            }
            else{
                
                response.send({'status':'success','data':users})
            }
        }
    })
})

router.get('/bookSubjects', (request, response) => {
    
    const statement = `select name from books GROUP BY subject;`
    db.query(statement, (error, users) => {
        if(error)
        {
            response.send({'status':'error', 'error':error})
        }
        else{
            if(users.length == 0)
            {
                response.send({'status':'error', 'error':'user not found'})
            }
            else{
                
                response.send({'status':'success','data':users})
            }
        }
    })
})

router.get('/booksname', (request, response) => {
    
    const statement = `select name from books;`
    db.query(statement, (error, users) => {
        if(error)
        {
            response.send({'status':'error', 'error':error})
        }
        else{
            if(users.length == 0)
            {
                response.send({'status':'error', 'error':'user not found'})
            }
            else{
                
                response.send({'status':'success','data':users})
            }
        }
    })
})

router.post('/addbook', (request, response) => {
    const {id, name, author, subject, price} = request.body

    const statement = `INSERT INTO books (id, name, author, subject, price) values 
    ('${id}','${name}','${author}','${subject}','${price}')`

    db.query(statement, (error, results) => {
        response.send(utils.createResult(error, results))
    })
})

router.get('/getbook/:userid', (request, response) => {
const {userid} = request.params

const statement = `select * from books where id='${userid}'`

db.query(statement, (error, results) => {
    response.send(utils.createResult(error, results))
})

})

router.delete('/deletebook/:userid', (request, response) => {
    const {userid} = request.params
    
    const statement = `delete from books where id='${userid}'`
    
    db.query(statement, (error, results) => {
        response.send(utils.createResult(error, results))
    })
    
    })

    router.put('/editbook/:userid', (request, response) => {
        const {userid} = request.params
        
        const statement = `update books 
        set name = '${name}', author = '${author}', subject = '${subject}'
        where id='${userid}'`
        
        db.query(statement, (error, results) => {
            response.send(utils.createResult(error, results))
        })
        
        })

module.exports = router