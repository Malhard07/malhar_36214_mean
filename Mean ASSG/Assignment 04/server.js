const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.json())

const bookRouter = require('./routes/books')
app.use('/book',bookRouter)

const userRouter = require('./routes/users')
app.use('/user',userRouter)

app.get('/', (request, response) =>{
    response.send('Welcome to Backend')
})

app.listen(4000,'0.0.0.0', () => {
    console.log(`Started Listening on port 4000`)
})
