
// const express = require('express')


// const app = express()


// app.get('/', (request, response) => {
//   console.log('GET / received')
//   response.end('from GET /')
// })

// app.get('/product', (request, response) => {
//   console.log('select * from product')
//   response.end('from GET /product')
// })

// app.post('/product', (request, response) => {
//   console.log('insert into product')
//   response.end('from POST /product')
// })


// app.put('/product', (request, response) => {
//   console.log('update product')
//   response.end('from PUT /product')
// })

// app.delete('/product', (request, response) => {
//   console.log('delete product')
//   response.end('from DELETE /product')
// })

// app.listen(3000, '0.0.0.0', () => {
//   console.log('server started on port 3000')
// })

const express = require('express')

const products = [
  { id: 1, title: 'product 1', description: 'this is product 1', price: 100 },
  { id: 2, title: 'product 2', description: 'this is product 2', price: 200 },
  { id: 3, title: 'product 3', description: 'this is product 3', price: 300 },
  { id: 4, title: 'product 4', description: 'this is product 4', price: 400 },
  { id: 5, title: 'product 5', description: 'this is product 5', price: 500 }
]

const app = express()

app.get('/', (request, response) => {
  response.end("<h1>My REST APIs</h1>")
})

app.get('/product', (request, response) => {

  response.setHeader('Content-Type', 'application/json')

  response.end(JSON.stringify(products))
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})