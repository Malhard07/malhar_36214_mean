
// const http = require('http')


// const server = http.createServer((request, response) => {
//   console.log('request has been received')

  
//   console.log(`url = ${request.url}`)

//   console.log(`method = ${request.method}`)

 
//   response.setHeader('Content-Type', 'text/html')

//   response.end('[{ "name": "steve", "company": "Apple" }, { "name": "bill", "company": "MS" }]')
// })


// server.listen(3000, '0.0.0.0', () => {
//   console.log('server started listening on port 3000')
// })

const http = require('http')

const server = http.createServer((request, response) => {
  console.log('request received')

  if (request.url == '/product') {
    if (request.method == 'GET') {
      console.log('select * from product')
    } else if (request.method == 'POST') {
      console.log('insert into product (...)')      
    } else if (request.method == 'PUT') {
      console.log('update product ...')
    } else if (request.method == 'DELETE') {
      console.log('delete product ...')
    }
  } 

  response.end('request processed')
})

server.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})