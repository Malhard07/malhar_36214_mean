import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  productService:ProductService

  products = []

  constructor(productService:ProductService) 
  {
    this.productService = productService
  }

  

  ngOnInit(): void {
    this.loadProducts()
  }

  loadProducts()
  {
    const request = this.productService.getproduct()
    request.subscribe(response => {
      if(response['status'] == 'success')
      {
        this.products = response['data']
      }
      else{
        alert('error loading products')
      }
    })
  }
}
