import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  httpClient: HttpClient
  url = "http://localhost:3000/product"
  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient
  }

  getproduct()
  {
    return this.httpClient.get(this.url)
  }

  createProduct(title: string, description: string, price: number, category: number, brand: number)
  {
    const body = {
      title: title,
      description: description,
      price: price,
      category: category,
      brand: brand
    }
    return this.httpClient.post(this.url, body)
  }
  
}
