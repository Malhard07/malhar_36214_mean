import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  httpClient:HttpClient
  url = "http://localhost:3000/brand"
  constructor(httpClient:HttpClient) 
  {
    this.httpClient = httpClient
  }

  getBrands()
  {
    return this.httpClient.get(this.url)
  }

}
