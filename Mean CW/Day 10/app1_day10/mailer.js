const nodemailer = require('nodemailer')
const config = require('./config')

function sendEmail(email, subject, body, callback) {
  const transport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.emailUser,
      pass: config.emailPassword
    }
  })

  const options = {
    from: config.emailUser,
    to: email,
    subject: subject,
    html: body
  }

  transport.sendMail(options, callback)
}

sendEmail('vedantkashyape@gmail.com', 'Welcome to My Project', 
`<h2 style="color: darkorange;">Howdy Vedant</h2>
<h2>Hello from Malhar</h2>
<div>This mail is sent as a test mail from Postman </div>
<div>Have a Great weekend</div>`)

// module.exports = {
//   sendEmail: sendEmail
// }