const express = require('express')
const utils = require('../../utils')
const db = require('../../db')

// multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: 'images/' })

const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /product:
 *   get:
 *     description: Edit previously added BRANDS 
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Brand addes successfully
 */

router.get('/', (request, response) => {
  const statement = `
      select p.id, p.title, p.description, 
        c.id as categoryId, c.title as categoryTitle,
        b.id as brandId, b.title as brandTitle,
        p.price, p.image, p.isActive from product p
      inner join category c on c.id = p.category
      inner join brand b on b.id = p.brand
  `
  db.query(statement, (error, data) => {
    if (error) {
      response.send(utils.createError(error))
    } else {
      // empty products collection
      const products = []

      // iterate over the collection and modify the structure
      for (let index = 0; index < data.length; index++) {
        const tmpProduct = data[index];
        const product = {
          id: tmpProduct['id'],
          title: tmpProduct['title'],
          description: tmpProduct['description'],
          price: tmpProduct['price'],
          isActive: tmpProduct['isActive'],
          brand: {
            id: tmpProduct['brandId'],
            title: tmpProduct['brandTitle']
          },
          category: {
            id: tmpProduct['categoryId'],
            title: tmpProduct['categoryTitle']
          }
        }
        products.push(product)
      }

      response.send(utils.createSuccess(products))
    }

  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------



router.post('/upload-image/:productId', upload.single('image'), (request, response) => {
  const {productId} = request.params
  const fileName = request.file.filename

  const statement = `update product set image = '${fileName}' where id = ${productId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

/**
 * @swagger
 *
 * /product/create:
 *   post:
 *     description: ADD Products 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: Title of Product.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Category
 *         description: Category of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Price
 *         description: Price of Product
 *         in: formData
 *         required: true
 *         type: int
 *       - name: Brand
 *         description: Brand of product 
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Product added successfully
 */

router.post('/create', (request, response) => {
  const {title, description, category, price, brand} = request.body
  const statement = `insert into product (title, description, category, price, brand) values (
    '${title}', '${description}', '${category}', '${price}', '${brand}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

/**
 * @swagger
 *
 * /:id:
 *   put:
 *     description: UPDATE Products 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: ID of Product.
 *         in: formData
 *         required: true
 *         type: int
 *       - name: title
 *         description: Title of Product.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Category
 *         description: Category of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Price
 *         description: Price of Product
 *         in: formData
 *         required: true
 *         type: int
 *       - name: Brand
 *         description: Brand of product 
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: Product added successfully
 */

router.put('/:id', (request, response) => {
  const {id} = request.params
  const {title, description, category, price, brand} = request.body
  const statement = `update product set 
      title = '${title}',
      description = '${description}',
      category = '${category}',
      price = '${price}',
      brand = '${brand}'
  where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

/**
 * @swagger
 *
 * /update-state/:id/:isActive:
 *   put:
 *     description: Disable/Enable Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: int
 *       - name: isActive
 *         description: Status of product either 0 or 1
 *         in: formData
 *         required: true
 *         type: int
 *     responses:
 *       200:
 *         description: login
 */

router.put('/update-state/:id/:isActive', (request, response) => {
  const {id, isActive} = request.params
  const statement = `update product set 
      isActive = ${isActive}
    where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

/**
 * @swagger
 *
 * /:id:
 *   delete:
 *     description: Delete Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: Delete product with id
 *         in: formData
 *         required: true
 *         type: int
 *     responses:
 *       200:
 *         description: login
 */


router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from product where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router