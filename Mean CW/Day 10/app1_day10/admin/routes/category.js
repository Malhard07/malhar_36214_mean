const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category:
 *   get:
 *     description: GET all categories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */

router.get('/', (request, response) => {
  const statement = `select id, title, description from category`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category:
 *   post:
 *     description: Add categories
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: Title of category.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Description
 *         description: Write the category's description
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */

router.post('/', (request, response) => {
  const {title, description} = request.body
  const statement = `insert into category (title, description) values ('${title}', '${description}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category:
 *   put:
 *     description: Edit categories
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: ID of brand.
 *         in: formData
 *         required: true
 *         type: int
 *       - name: title
 *         description: Title of category.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: Description
 *         description: Write the category's description
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */

router.put('/:id', (request, response) => {
  const {id} = request.params
  const {title, description} = request.body
  const statement = `update category set title = '${title}', description = '${description}' where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category:
 *   delete:
 *     description: Edit categories
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: ID of brand.
 *         in: formData
 *         required: true
 *         type: int
 *     responses:
 *       200:
 *         description: login
 */

router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from category where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router