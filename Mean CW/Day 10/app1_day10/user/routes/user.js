const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const crypto = require('crypto-js')
const mailer = require('../../mailer')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------



// ---------------------------------------
//                  POST
// ---------------------------------------

router.post('/signup', (request, response) => {
  const {firstName, lastName, email, password} = request.body
  
  const statement = `insert into user (firstName, lastName, email, password) values (
    '${firstName}', '${lastName}', '${email}', '${crypto.SHA256(password)}'
  )`
  db.query(statement, (error, data) => {

    mailer.sendEmail(email, 'Welcome to mystore', '<h1>welcome</h1>',  (error, info) => {
      console.log(error)
      console.log(info)
      response.send(utils.createResult(error, data))
    })

  })
})



// ---------------------------------------
//                  PUT
// ---------------------------------------




// ---------------------------------------
//                  DELETE
// ---------------------------------------



module.exports = router