function function1() {

    // array of strings
    const countries = ["india", "usa", "uk"]
    console.log(`${countries}`)
    console.log(`type of countries = ${typeof(countries)}`)
  
    // array of objects [JSON]
    const persons = [
      { "name": "person1", age: 40 },
      { "name": "person2", age: 20 }
    ]
    
    //console.log(`${persons}`)
    // We CANNOT write OBJECT in ${} else it will give o/p as [object:object]. We have to write it 
    // in NO QUOTES as in LINE NO. 17 
    console.log(persons)
    console.log(`type of persons = ${typeof(persons)}`)
  }
  
  //function1()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------


  function convertCar(car) {
    // construct a new object with model and company keys
    // WHILE USING OBJECT THE USE OF "=" IS NOT ALLOWED. ONLY USE ":" EVERYWHERE, EVEN IN RETURN VALUE
    // USE ":" 
    return { model: car["model"], company: car["company"] }
  }
  
  function function7() {
    const cars = [
      { model: "i20", price: 7.5, company:"hyundai",color: "space gray"},
      { model: "nano", price: 2.5, compan: "tata", color: "yellow"},
      { model: "x5", price: 35.5, company:"BMW",    color: "dark blue"}
    ]
  
    const newCars = cars.map(convertCar)
    console.log(cars)
    console.log(newCars)
  }

  // function7()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------
  
function person(p)
{
    return{
        name: p["name"], age: p["age"]
    }
}


function function8() {
    const persons = [
      { name: "person1", email: "person1@test.com", age: 40 },
      { name: "person2", email: "person2@test.com", age: 45 },
      { name: "person3", email: "person3@test.com", age: 50 },
      { name: "person4", email: "person4@test.com", age: 46 }
    ]

    const person_data = persons.map(person)
    console.log(person_data)

}

//function8()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------


function cheap_cars(car)
{
    if(car["price"]<10)
    {
        return car;
    }
}

function function6() {
    const cars = [
      { model: "i20", price: 7.5, company: "hyundai", color: "space gray"},
      { model: "nano", price: 2.5, company: "tata", color: "yellow"},
      { model: "x5", price: 35.5, company: "BMW", color: "dark blue"}
    ]

    const affordable_cars = cars.filter(cheap_cars)
    console.log(affordable_cars)
}

//function6()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------
function can_square(p1)
{
        if(p1 % 2 != 0)
    {
        let x = p1*p1; 
        return x
    }
}

function can_cube(p1)
{
    if(p1%2==0)
    {
        let x = p1*p1*p1 
        return x
    }
}

function function1() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    // square of odd numbers
    const square = numbers.map(can_square)
    console.log(`${square}`)
  
    // cube of even numbers
    const cube = numbers.map(can_cube)
    console.log(`${cube}`)

  }

  //function1()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------

  function function1() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  const square = (number) => {
    return number * number
  }
  const squares = numbers.map(square)
  console.log(squares)
}

// function1()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------
function function2() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  const squares = numbers.map((number) => {
    return number * number
  })
  console.log(squares)

  const cubes = numbers.map((number) => {
    return number * number * number
  })
  console.log(cubes)
}

// function2()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------

function function3() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  const evens = numbers.filter((number) => {
    return number % 2 == 0
  })
  console.log(evens)

  const odds = numbers.filter((number) => {
    return number % 2 != 0
  })
  console.log(odds)
}

//function3()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------

function function1() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  const square = (number) => {
    return number * number
  }
  const squares = numbers.map(square)
  console.log(squares)
}

// function1()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------

function function2() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  const squares = numbers.map((number) => {
    return number * number
  })
  console.log(squares)

  const cubes = numbers.map((number) => {
    return number * number * number
  })
  console.log(cubes)
}

// function2()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------

function function3() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  const evens = numbers.filter((number) => {
    return number % 2 == 0
  })
  console.log(evens)

  const odds = numbers.filter((number) => {
    return number % 2 != 0
  })
  console.log(odds)
}

function3()
//   ----------------------------------------------------------------------------------------------
//   ----------------------------------------------------------------------------------------------