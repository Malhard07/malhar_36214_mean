//--------------------------------------------------------------------------------------------------
// OBJECT USING JSON
//--------------------------------------------------------------------------------------------------

//  const person = {name:'Malhar' ,address: 'Bombay' ,hobby: 'acting'}
//  console.log(`name is = ${person['name']}`)
//  console.log(`address is = ${person['address']}`)
//  console.log(`hobby is = ${person['hobby']}`)

//  const persons = {name:'Naruto', address:'Konoha', Designation:'Hokage'}
//  console.log(`name : ${persons.name}`)
//  console.log(`address : ${persons.address}`)
//  console.log(`Designation : ${persons.Designation}`)

// function canVotePerson() {
//     //console.log(`inside canVotePerson`)
//     //console.log(this)
//     if (this['age'] >= 18) {
//       console.log(`person ${this['name']} is eligible for voting`)
//     } else {
//       console.log(`person ${this['name']} is NOT eligible for voting`)
//     }
//   }
  
//   function function7() {
//     const p1 = { name: 'person1', email: 'person1@test.com', age: 40 }
  
//     // PoP
    // canVotePerson(p1)
  
//     // function alias
    // const myCanVote = canVotePerson
    // myCanVote(p1)
  
    // console.log(`name = ${p1.name}`)
  
//     // function alias added in the person object
    // p1.canVote = canVotePerson
    //console.log(p1)
  
//     // OOP
   
//     p1.canVote()
   
//   }
  
//    function7()

// function is_hokage()
// {
//     if(this['name']=='Naruto'||this['name']=='Kakashi')
//     {
//         console.log(`${this['name']} is HOKAGE`)
//     }

//     else
//     {
//         console.log(`${this['name']}is NOT HOKAGE`)
//     }
// }
// function function6()
// {
// const p1 = {name:'Naruto', level:'jonin', village:'leaf'}
// const p2 = {name:'Kakashi', level:'jonin', village:'leaf'}
// const p3 = {name:'Shikamaru', level:'jonin', village:'leaf'}
// const p4 = {name:'Sasuke', level:'jonin', village:'rogue'}

// p1.hokage = is_hokage
// p2.hokage = is_hokage
// p3.hokage = is_hokage
// p4.hokage = is_hokage

// p1.hokage()
// p2.hokage()
// p3.hokage()
// p4.hokage()
// }

// function6()


// // ------------------------------------------------------------------------------------------------
// // ------------------------------------------------------------------------------------------------
// // ------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------------------
//object USING Object
//---------------------------------------------------------------------------------------------------

// function printDetails() {
//     console.log(`name: ${this.name}`)
//     console.log(`age: ${this.age}`)
//     console.log(`email: ${this.email}`)
//   }
  
//   function function3() {
//     const p1 = new Object()
//     p1.name = 'person1'
//     p1.email = 'person1@test.com'
//     p1.age = 40
//     p1.printDetails = printDetails
//     console.log(p1)
//     p1.printDetails()
  
//     const p2 = new Object()
//     p2.name = 'person2'
//     p2.email = 'person2@test.com'
//     p2.age = 30
//     p2.printDetails = printDetails
    
//     console.log(p2)
//     p2.printDetails()
  
//   }
  
//   function3()

// // ------------------------------------------------------------------------------------------------
// // ------------------------------------------------------------------------------------------------
// // ------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
//object USING CONSTRUCTOR FUNCTION
//---------------------------------------------------------------------------------------------------

function Mobile(model, company, price) {
    this.model = model
    this.company = company
    this.price = price
  }

  Mobile.prototype.toString = function() {
    return `Mobile [ model: ${this.model}, company: ${this.company}, price: ${this.price}]`
  }
  
  Mobile.prototype.canAfford = function() {
    if (this.price >= 30000) {
      console.log(`${this.model} is NOT affordable`)
    } else {
      console.log(`${this.model} is affordable`)
    }
  }
  
  const m1 = new Mobile('iPhone XS Max', 'Apple', 144000)
  
  
  const m2 = new Mobile('Note Pro 5', 'Xiomi', 15000)
  
  const m3 = new Mobile('z10', 'BlackBerry', 43000)
  
 
  
  m1.canAfford()
  m2.canAfford()
  m3.canAfford()

function Person(name, age, email) {
    this.name = name
    this.email = email
    this.age = age
  
   
  }
  
  Person.prototype.canVote = function() {
    if (this.age >= 18) {
      console.log(`${this.name} is eligible for voting`)
    } else {
      console.log(`${this.name} is NOT eligible for voting`)
    }
  }
  
  const p1 = new Person('person1', 40, 'person1@test.com')
  console.log(p1)
  console.log('' + p1)
  console.log(p1.toString())

  
  p1.canVote()
  
  const p2 = new Person('person2', 20, 'person2@test.com')
  console.log(p2)
  console.log(`${p2}`)
  console.log(p2.toString())
 
  p2.canVote()
  // p2.printDetails()
// // ------------------------------------------------------------------------------------------------
// // ------------------------------------------------------------------------------------------------
// // ------------------------------------------------------------------------------------------------
