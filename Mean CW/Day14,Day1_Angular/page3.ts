import { Person } from './person'
import * as page1 from './page1'

const person = new Person('person1', 'pune', 10)
person.printInfo()
person.canVote()

page1.add(30, 50)
page1.divide(40, 10)
console.log(`pi = ${page1.pi}`)
