import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor() { }

  firstname = 'Malhar'
  company = 'Google'

   PrimaryData = {
     Name : "Malhar",
     Company : "Google",
     Designation : "Full Stack Developer"
   }

  ngOnInit(): void {
  }

}
