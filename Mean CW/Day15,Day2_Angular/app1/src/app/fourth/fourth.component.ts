import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fourth',
  templateUrl: './fourth.component.html',
  styleUrls: ['./fourth.component.css']
})
export class FourthComponent implements OnInit {

  constructor() { }

  number = 1
  clicked()
  {
    alert("Button Clicked")
  }

  changecolor(colour)
  {
    this.number = colour
  }

  ngOnInit(): void {
  }

}
