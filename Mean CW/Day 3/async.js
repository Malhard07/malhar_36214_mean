

function asynchrousReadFile() {
    console.log('reading file started')
  
    // starts a thread to perform the read operation
    fs.readFile('./file1.txt', (error, data) => {
      // the reading is finished
  
      console.log('file reading finished')
  
      if (error) {
        console.log(`error: ${error}`)
      } else {
        console.log(`data = ${data}`)
      }
      console.log('bye bye')
    })

    asynchrousReadFile()


function funtion1() {
  console.log('download started')
  setTimeout(() => {
    console.log('download finished')
  }, 5000)

  // perform mathematical operation
  console.log('performing mulitplication')
  const result =  244534433534 * 23424243242243
  console.log(`answer = ${result}`) 
  
  console.log('performing another task')
  setTimeout(() => {
    console.log('another task finished')
  }, 10000)
}

// funtion1()


function myReadFile(path, func) {
  // func =  (error, data) => { .. }
  const data = fs.readFileSync(path)
  setTimeout(() => {
    func(null, data)
  }, 10000)

