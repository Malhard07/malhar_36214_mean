const fs = require('fs')


function synchrousReadFile() {
  // reading the file
  console.log(`file reading started`)

  try {
    // blocking call/API
    const data = fs.readFileSync('./file2.txt')
    console.log('file reading finished')
    console.log(`data = ${data}`)
    console.log('bye bye')
  } catch(ex) {
    console.log(`exception: ${ex}`)
  }

  // mathematical calculation
  console.log('peforming multiplication')
  const result = 23412424524524 * 3453245245232543
  console.log(`result = ${result}`)

  // mathematical calculation
  console.log('peforming division')
  const result2 = 23412424524524 / 3453245245232543
  console.log(`result2 = ${result2}`)
}

 synchrousReadFile()




  // perform mathematical operation
  console.log('performing mulitplication')
  const result =  244534433534 * 23424243242243
  console.log(`answer = ${result}`)

  // mathematical calculation
  console.log('peforming division')
  const result2 = 23412424524524 / 3453245245232543
  console.log(`result2 = ${result2}`)

