const express = require('express')
const app = express()

const bodyParser = require('body-parser')
app.use(bodyParser.json())

const noteRouter = require('./notes') 
app.use('/note',noteRouter)

const userRouter = require('./users')
app.use('/user', userRouter)

app.listen(3000, '0.0.0.0', () => {
    console.log('SERVER Has STARTED LISTNING On PORT 3000')
})