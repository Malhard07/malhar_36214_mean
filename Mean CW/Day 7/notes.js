const express = require('express')
const router = express.Router()
const db = require('./db')
const utils = require('./utils')
const { request, response } = require('express')


router.get('/:Note_ID', (request,response) => {

    const {Note_ID} = request.params
    const statement = `select * from sticky_notes where Note_ID = '${Note_ID}'`

    db.query(statement, (error, users) => {
        if(error)
        {
            response.send(error)
        }
        else{
            if(users.length == 0)
            {
                response.send('User Does Not EXIST')
            }
            else{
                const user = users[0]
                const result = {"status":''}
                result['status'] = "SUCCESS"
                result['data'] = user
                response.send(result)
            }
        }
    })

})

router.post('/', (request,response) => {
    const {Title, Category, Content} = request.body

    const statement = `INSERT INTO sticky_notes (Title, Category, Content) 
    VALUES ('${Title}','${Category}','${Content}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.put('/:Note_ID', (request,response) => {
    const { Note_ID } = request.params
    const { contents } = request.body
    const statement = `update note set Contents = '${Contents}' where Note_ID = '${Note_ID}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
})

router.delete('/:Note_ID', (request, response) => {
    const { Note_ID } = request.params
    const statement = `delete from note where Note_ID = '${Note_ID}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

module.exports = router