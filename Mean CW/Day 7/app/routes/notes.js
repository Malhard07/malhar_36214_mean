const express = require('express')
const db = require('../db')
const utils = require('../utils')
const jwt = require('jsonwebtoken')

const router = express.Router()

router.get('/', (request, response) => {
  const token = request.headers['token']
  try {
    const data = jwt.verify(token, '1234567890')
    const statement = `select id, userId, contents, timestamp from note where userId = ${data.id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
  
})

router.post('/', (request, response) => {

  const token = request.headers['token']
  try {

    const data = jwt.verify(token, '1234567890')
    const { contents } = request.body
    const statement = `insert into note (contents, userId) values ('${contents}', '${data.id}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }

})

router.put('/:id', (request, response) => {
  const token = request.headers['token']

  try {
    const data = jwt.verify(token, '1234567890')
    const { id } = request.params
    const { contents } = request.body
    const statement = `update note set contents = '${contents}' where id = '${id}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})

router.delete('/:id', (request, response) => {
  const token = request.headers['token']
  try {

    const data = jwt.verify(token, '1234567890')
    const { id } = request.params
    const statement = `delete from note where id = '${id}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})


module.exports = router