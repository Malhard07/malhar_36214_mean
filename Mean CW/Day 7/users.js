const express = require('express')
const router = express.Router()
const db = require('./db')
const utils = require('./utils')
const crypto = require('crypto-js')
const { request, response } = require('express')


router.get('/profile/:id', (request, response) => {

const { id } = request.params

const statement = `select email, name, phone from user where id = '${id}'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        // user does not exist
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        // user exists
        const user = users[0]
        response.send({ status: 'success', data: user })
      }
    }
  })

})



router.post('/signup', (request, response) => {
   const {name, phone, email, password} = request.body
   
   const encryptPassword = crypto.SHA256(password)

    const statement = `INSERT INTO user (name, phone, email, password) values
     ('${name}','${phone}','${email}','${encryptPassword}')`

        db.query(statement, (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.post('/signin', (request, response) => {
    const {email, password} = request.body

    const cryptoPassword = crypto.SHA256(password)
    
    const statement = `select name, phone, email from user 
    where password = '${cryptoPassword}' and email = '${email}'`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

module.exports = router