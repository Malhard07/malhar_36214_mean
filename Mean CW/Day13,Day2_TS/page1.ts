class Leaf {

    private firstName: string
    private Clan: string
    private Rank: string
    private Kekkai_Genkai: string
    private No_of_missions: number

    getfirstName(): string{
        return this.firstName
    }
    setfirstName(firstName: string){
        this.firstName = firstName
    }
    
    getClan(): string{
        return this.Clan
    }
    setClan(Clan: string){
        this.Clan = Clan
    }

    getRank(){
        return this.Rank
    }
    setRank(Rank: string){
        this.Rank = Rank   
    }

    getKekkai_Genkai(){
        return this.Kekkai_Genkai
    }
    setKekkai_Genkai(Kekkai_Genkai: string){
        this.Kekkai_Genkai = Kekkai_Genkai
    }

    getNo_of_missions(){
        return this.No_of_missions
    }
    setNo_of_missions(No_of_missions){
        this.No_of_missions = No_of_missions
    }

}

const l1 = new Leaf()
l1.setfirstName('Naruto')
l1.setClan('Uzumaki')
l1.setNo_of_missions(450)
l1.setKekkai_Genkai('No')
l1.setRank('Hokage')

console.log(l1)