1. Getter Setter Implemented in page1.ts

2. Constructor for Super class and sub class implemented in page2.ts 
  - Note: Base class constructor is called first when derived class object is created BUT in TS 
        we have to call super class constructor EXPLICITELY else error occurs. It can be called 
        using 'super.constructor_name' 

3. In page3.ts Getter and setter are IMPLEMENTED as objects.
    -- While doing so "--target es5" should be added 
        ex. ->  tsc --target es5 page3.ts;node page3.js

5. In page6.ts Inheritance is Implemented between 2 class Person and player using extends

6. In page7.ts Virtual function is created

7. In page8.ts interface is created i.e each member function = 0. And upcasting is done
    
    This is upcasting -->>
    function function1(drawable: IDrawable) {
    drawable.draw()
    drawable.erase()
    }// Now only base class functionality can be accessed by derived class object
