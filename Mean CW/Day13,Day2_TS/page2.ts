// base class 
// super class
class Person { 
    protected name: string = ''
    protected address: string = ''
    protected age: number = 0
  
    public constructor(name: string, address: string, age: number) {
        console.log('inside person')
      this.name = name
      this.address = address
      this.age = age
    }
  }
  
  // derrived class
  // subclass
  class Player extends Person {
    team: string = ''
  
    public constructor(name: string, address: string, age: number, team: string) {
      // calling super class (Person) constructor
      super(name, address, age)
      console.log('inside person')
      this.team = team
    }
  
    public printInfo() {
      console.log(`name = ${this.name}`)
      console.log(`address = ${this.address}`)
      console.log(`age = ${this.age}`)
      console.log(`team = ${this.team}`)
    }
  }
  
  const person1 = new Person("person1", "pune", 20)
  console.log(person1)
  
  const player1 = new Player('player1', 'mumbai', 18, 'india')
  console.log(player1)
  player1.printInfo()