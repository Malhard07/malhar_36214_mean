class Person{

    private name: string
    private age: number
    private address: string

    constructor(name: string, age: number, address: string)
    {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    set Name(name: string)
    {
        this.name = name
    }
    get Name()
    {
        return this.name
    }

    set Age(age: number)
    {
        this.age = age
    }
    get Age()
    {
        return this.age
    }


    set Address(address: string)
    {
        this.address = address
    }
    get Address()
    {
        return this.address
    }
}

class Player extends Person{

    private team: string

    constructor(name: string, age: number, address: string, team: string)
    {
        super(name,age,address)
        this.team = team
    }
    set Team(team: string)
    {
        this.team = team
    }
    get Team()
    {
        return this.team
    }
}

const p1 = new Player('Malhar', 20, 'Mumbai', 'India')
console.log(p1)

const p2 = new Person('Malhar',18, 'Pune')
console.log(p2)