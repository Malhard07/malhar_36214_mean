var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(name, age, address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }
    Object.defineProperty(Person.prototype, "Name", {
        get: function () {
            return this.name;
        },
        set: function (name) {
            this.name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Age", {
        get: function () {
            return this.age;
        },
        set: function (age) {
            this.age = age;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Address", {
        get: function () {
            return this.address;
        },
        set: function (address) {
            this.address = address;
        },
        enumerable: false,
        configurable: true
    });
    return Person;
}());
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player(name, age, address, team) {
        var _this = _super.call(this, name, age, address) || this;
        _this.team = team;
        return _this;
    }
    Object.defineProperty(Player.prototype, "Team", {
        get: function () {
            return this.team;
        },
        set: function (team) {
            this.team = team;
        },
        enumerable: false,
        configurable: true
    });
    return Player;
}(Person));
var p1 = new Player('Malhar', 20, 'Mumbai', 'India');
console.log(p1);
var p2 = new Person('Malhar', 18, 'Pune');
console.log(p2);
