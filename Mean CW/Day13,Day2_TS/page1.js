var Leaf = /** @class */ (function () {
    function Leaf() {
    }
    Leaf.prototype.getfirstName = function () {
        return this.firstName;
    };
    Leaf.prototype.setfirstName = function (firstName) {
        this.firstName = firstName;
    };
    Leaf.prototype.getClan = function () {
        return this.Clan;
    };
    Leaf.prototype.setClan = function (Clan) {
        this.Clan = Clan;
    };
    Leaf.prototype.getRank = function () {
        return this.Rank;
    };
    Leaf.prototype.setRank = function (Rank) {
        this.Rank = Rank;
    };
    Leaf.prototype.getKekkai_Genkai = function () {
        return this.Kekkai_Genkai;
    };
    Leaf.prototype.setKekkai_Genkai = function (Kekkai_Genkai) {
        this.Kekkai_Genkai = Kekkai_Genkai;
    };
    Leaf.prototype.getNo_of_missions = function () {
        return this.No_of_missions;
    };
    Leaf.prototype.setNo_of_missions = function (No_of_missions) {
        this.No_of_missions = No_of_missions;
    };
    return Leaf;
}());
var l1 = new Leaf();
l1.setfirstName('Naruto');
l1.setClan('Uzumaki');
l1.setNo_of_missions(450);
l1.setKekkai_Genkai('No');
l1.setRank('Hokage');
console.log(l1);
