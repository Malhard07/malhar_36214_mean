var Person = /** @class */ (function () {
    // public constructor() {
    //   console.log("inside constructor")
    //   this._name = ''
    //   this._age = 0
    // }
    // constructor
    function Person(name, age) {
        if (name === void 0) { name = 'Malhar'; }
        if (age === void 0) { age = 10; }
        this._name = name;
        this._age = age;
    }
    Object.defineProperty(Person.prototype, "name", {
        // getter
        get: function () { return this._name; },
        // setter
        set: function (name) { this._name = name; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "age", {
        get: function () { return this._age; },
        set: function (age) { this._age = age; },
        enumerable: false,
        configurable: true
    });
    // facilitator or utility
    Person.prototype.canVote = function () {
        if (this._age >= 18) {
            console.log(this._name + " is eligible for voting");
        }
        else {
            console.log(this._name + " is NOT eligible for voting");
        }
    };
    return Person;
}());
var p1 = new Person();
console.log(p1);
