var Person = /** @class */ (function () {
    function Person() {
    }
    Object.defineProperty(Person.prototype, "_name", {
        set: function (name) {
            this.name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Name", {
        get: function () {
            return this.name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "_age", {
        set: function (age) {
            this.age = age;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Age", {
        get: function () {
            return this.age;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "_address", {
        set: function (address) {
            this.address = address;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Address", {
        get: function () {
            return this.address;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "_phone", {
        set: function (phone) {
            this.phone = phone;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Person.prototype, "Phone", {
        get: function () {
            return this.phone;
        },
        enumerable: false,
        configurable: true
    });
    return Person;
}());
var p1 = new Person();
p1._name = 'Malhar';
p1._age = 23;
p1._address = 'Mumbai';
p1._phone = 845123697;
console.log(p1);
