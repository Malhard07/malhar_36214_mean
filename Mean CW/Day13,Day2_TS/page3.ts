class Person{
    
    private name: string
    private age: number
    private address: string
    private phone: number

    set _name(name: string){
        this.name = name
    }
    get Name(){
        return this.name
    }

    set _age(age: number){
        this.age = age
    }
    get Age(){
        return this.age
    }

    set _address(address: string){
        this.address = address
    }
    get Address(){
       return this.address
    }

    set _phone(phone: number){
        this.phone = phone
    }
    get Phone(){
        return this.phone
    }
}

const p1 = new Person()
p1._name = 'Malhar'
p1._age = 23
p1._address = 'Mumbai'
p1._phone = 845123697

console.log(p1)