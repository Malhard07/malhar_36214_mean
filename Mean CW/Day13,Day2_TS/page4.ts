class Person {
    private _name: string 
    private _age: number
  
    // public constructor() {
    //   console.log("inside constructor")
    //   this._name = ''
    //   this._age = 0
    // }
  
    // constructor
    public constructor(name: string = 'Malhar', age: number = 10) {
      this._name = name
      this._age = age
    }
  
    // setter
    public set name(name: string) { this._name = name }
    public set age(age: number) { this._age = age }
  
    // getter
    public get name() { return this._name }
    public get age() { return this._age }
  
    // facilitator or utility
    public canVote() {
      if (this._age >= 18) { console.log(`${this._name} is eligible for voting`) }
      else { console.log(`${this._name} is NOT eligible for voting`) }
    }
  }
  
  const p1 = new Person()
console.log(p1)  

  