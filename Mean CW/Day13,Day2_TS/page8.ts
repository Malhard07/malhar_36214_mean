interface Run {
    run()
  }
  
  class Animal implements Run {
    run() {
      console.log('animal is running')
    }
  }
  
  // contract between service provider and service consumer
  interface IDrawable {
    draw()
    erase()
  }
  
  // service provider
  class Rectangle implements IDrawable {
  
    draw() {
      console.log('drawing rectangle')
    }
    erase() {
      console.log('erasing rectangle')
    }
  
    rectangle() {
      console.log('inside rectangle')
    }
    
  }
  
  class Circle implements IDrawable {
  
    draw() {
      console.log('drawing circle')
    }
    erase() {
      console.log('erasing circle')
    }
    
  }
  
  class Square implements IDrawable {
  
    draw() {
      console.log('drawing square')
    }
    erase() {
      console.log('erasing square')
    }
    
  }
  
  
  function function1(drawable: IDrawable) {
    drawable.draw()
    drawable.erase()
  }
  
  
  //       type of reference = type of object
  const drawable1: IDrawable = new Rectangle()
  function1(drawable1)
  
  const drawable2: IDrawable = new Circle()
  function1(drawable2)
  
  const drawable3: IDrawable = new Square()
  function1(drawable3)