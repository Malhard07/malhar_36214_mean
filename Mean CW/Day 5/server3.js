const express = require('express')
const app = express()
const mysql = require('mysql')
const bodyparser = require('body-parser')

app.use(bodyparser.json())

app.get('/', (request,response) => {
    const connection = mysql.createConnection({
        host:'127.0.0.1',
        user:'root',
        password:'manager',
        database:'dac_db'
    })
    connection.connect()
    const statement = `CREATE TABLE Ninjas
    (
        ID INT AUTO_INCREMENT,
        Name varchar(50),
        Village varchar(50),
        Post varchar(10),
        Chakra_Nature varchar(50),
        Skills varchar(50),
        Kekkei_Genkai varchar(5),
        PRIMARY KEY(ID)
        
    )`

    connection.query(statement, (error,result) => {
        connection.end()

        if(error)
        {
            response.end(error)
        }

        else{
            response.send(result)
        }
    })
})

app.get('/ninja', (request,response) => {

const connection = mysql.createConnection({
    host:'127.0.0.1',
    user:'root',
    password:'manager',
    database:'dac_db'

})

const statement = `select * from Ninjas`

connection.query(statement,(error,result) => {
    connection.end()
    if(error)
    {
        console.log(error)
    }
    else{
        response.send(result)
    }
})

})
  
  app.post('/ninja', (request,response) => {

    const connection = mysql.createConnection({
        host:'127.0.0.1',
        user:'root',
        password:'manager',
        database:'dac_db'
    })

    const statement = `INSERT INTO Ninjas
    (Name, Village, Post, Chakra_Nature, Skills, Kekkei_Genkai)
    VALUES
    ('${request.body.Name}',
    '${request.body.Village}',
    '${request.body.Post}',
    '${request.body.Chakra_Nature}',
    '${request.body.Skills}',
    '${request.body.Kekkei_Genkai}'
    )`

    connection.query(statement, (error,result) => {

        connection.end()
        if(error)
        {
            console.log(error)
        }
        else{
            response.send(result)
        }
    })

  })

  app.put('/ninja', (request,response) => {

const connection = mysql.createConnection({
    host:'127.0.0.1',
    user:'root',
    password:'manager',
    database:'dac_db'
})

const statement = `UPDATE Ninjas
SET 
Name = '${request.body.Name}',
Village = '${request.body.Village}',
Post = '${request.body.Post}',
Chakra_Nature = '${request.body.Chakra_Nature}',
Skills = '${request.body.Skills}',
Kekkei_Genkai = '${request.body.Kekkei_Genkai}'
WHERE  ID = ${request.body.ID}
`

connection.query(statement, (error,result) => {
    connection.end()
    if(error)
    {
        console.log(error)
    }

    else{
        response.send(result)
    }
})

  })

  app.delete('/ninja', (request,response) => {
     const connection = mysql.createConnection({
         host:'127.0.0.1',
         user:'root',
         password:'manager',
         database:'dac_db'
     })

     const statement = `DELETE FROM Ninjas WHERE ID = ${request.body.ID}`
     
     connection.query(statement, (error,result) => {
         connection.end()
         if(!error)
         {
             response.send(result)
         }

         else{
             console.log(error)
         }

     })
  })

  app.listen(3000,'0.0.0.0',() => {
    console.log('server started on port 3000')
  })