import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  employees = [
    {
      id: 1,
      name: 'employee 1',
      department: 'computer',
      salary: 15.50,
      role: 'developer'
    },
    {
      id: 2,
      name: 'employee 2',
      department: 'computer',
      salary: 10.50,
      role: 'tester'
    },
    {
      id: 3,
      name: 'employee 3',
      department: 'computer',
      salary: 25.50,
      role: 'architect'
    },
    {
      id: 4,
      name: 'employee 4',
      department: 'account',
      salary: 10.50,
      role: 'accountant'
    }
  ]


}
