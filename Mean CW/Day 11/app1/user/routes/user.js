const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const crypto = require('crypto-js')
const mailer = require('../../mailer')
const uuid = require('uuid')
const fs = require('fs')
const router = express.Router()
// ---------------------------------------
//                  GET
// ---------------------------------------

router.get('/activate/:token', (request, response) => {

const {token} = request.params

const statement = `update user set active = 1, activationToken = '' 
where activationToken = '${token}'`

db.query(statement, (error, result) => {

  let body = '' + fs.readFileSync('user/routes/templates/activation_result.html')
  response.header('Content-Type', 'text/html')
  response.send(body)
})

})

// ---------------------------------------
//                  POST
// ---------------------------------------

router.post('/signup', (request, response) => {
  const {firstName, lastName, email, password} = request.body

  const activationToken = uuid.v4()
  const activationLink = `http://localhost:4000/user/activate/${activationToken}`
  let body = '' + fs.readFileSync('user/routes/templates/send_activation_link.html')
  body = body.replace('firstName', firstName)
  body = body.replace('activationLink', activationLink)
  
  const statement = `INSERT INTO user (firstName, lastName, email, password, activationToken)
  values(
  '${firstName}','${lastName}','${email}','${crypto.SHA256(password)}','${activationToken}')`

  
  db.query(statement, (error, data) => {

    mailer.sendEmail(email, 'Welcome to mystore', body,  (error, info) => {
      response.send(utils.createResult(error, data))
    })

  })
  
})


router.post('/signin', (request, response) => {
  const {email, password} = request.body
  
  const statement = `select * from user 
  where 
  email = '${email}' and password = '${crypto.SHA256(password)}'`
  const result = {status: ''}
  db.query(statement, (error, users) => {
    if(error)
    {
      response.send(result['status'] = 'error', result['error'] = error)
    }
    else{
      if(users.length == 0)
      {
        response.send('User Not Found')
      }
      else{
        const user = users[0]
        response.send(utils.createSuccess(user))
      }
    }
  })
})

router.post('/resetpass', (request, response) => {
  const {email, password} = request.body

  const statement = `UPDATE user set password = '${crypto.SHA256(password)}' where email = '${email}'`

  db.query(statement, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})



// ---------------------------------------
//                  PUT
// ---------------------------------------




// ---------------------------------------
//                  DELETE
// ---------------------------------------



module.exports = router