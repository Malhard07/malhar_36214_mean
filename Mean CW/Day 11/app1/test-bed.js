const uuid = require('uuid')

const token1 = uuid.v1()
//const token3 = uuid.v3()
const token4 = uuid.v4()
//const token5 = uuid.v5()

console.log(`token 1 = ${token1}`)
//console.log(`token 3 = ${token3}`)
console.log(`token 4 = ${token4}`)
//console.log(`token 5 = ${token5}`)
