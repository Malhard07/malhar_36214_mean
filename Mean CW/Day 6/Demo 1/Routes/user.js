const express = require('express')
const router = express.Router()

router.get('/user',(request,response) => {
    console.log(`FROM GET/user`)
    response.send('SUCCESSFUL FROM GET/user')
})

router.post('/user', (request,response) => {
    console.log(`FROM POST/user`)
    response.send('SUCCESSFUL FROM POST/user')
})

 module.exports = router