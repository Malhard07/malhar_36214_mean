const express = require('express')
const router = express.Router()

const db = require('../db.js')

router.post('/user/signup',(request,response) => {
    
    const First_Name = request.body.First_Name
    const Last_Name = request.body.Last_Name
    const Email = request.body.Email
    const Password = request.body.Password
    const Phone = request.body.Phone

    const statement = `INSERT INTO notes (First_Name, Last_Name, Email, Password, Phone)values 
    ('${First_Name}', '${Last_Name}', '${Email}', '${Password}', '${Phone}')`

    db.query(statement, (error, dbResult) => {
        const result = {status: ''}
        if(error)
        {
            result['status'] = 'error'
            console.log(error)
        }
        else
        {
            result['status'] = 'Success'
            result['data'] = {
                First_Name:dbResult['First_Name'],
                Last_Name:dbResult['Last_Name']

            }
        }
        response.send(result)
    })

})

router.post('/user/signin', (request,response) => {

    const email = request.body.Email
    const password = request.body.Password

    const statement = `SELECT * FROM notes WHERE Email = '${email}' and Password = '${password}'`
    db.query(statement, (error,users) => {
        const result = {status:''}
console.log(users.length)
        if(users.length == 0)
        {
            result['status'] = 'error'
            result['error'] = 'User not Matched/Found'
        }
        
        else{
           const user = users[0]
            result['status'] = 'Success'
            result['data'] = 
            {
                First_Name: user['First_Name'],
                Last_Name: user['Last_Name'],
                Email: user['Email'],
                Phone: user['Phone']

            }
        }
        response.send(result)
    })
    
})


router.put('/user/update', (request, response) => {
    
    const First_Name = request.body.First_Name
    const Last_Name = request.body.Last_Name
    const Email = request.body.Email
    const Password = request.body.Password
    const Phone = request.body.Phone
    const ID = request.body.ID

    const statement = `UPDATE notes 
                       SET 
                       First_Name = '${First_Name}',
                       Last_Name = '${Last_Name}',
                       Email = '${Email}',
                       Password = '${Password}',
                       Phone = '${Phone}'
                       WHERE ID = '${ID}'`

    db.query(statement, (error, dbResult) => {
        const result = {status:''}

        if(error)
        {
            result['status'] = 'error'
            response.send(error)
        }

        else{
            result['status'] = 'Success'
        }
        response.send(dbResult)
    })                   

})

 module.exports = router