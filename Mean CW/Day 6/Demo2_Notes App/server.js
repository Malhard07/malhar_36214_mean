const express = require('express')
const app = express()

const bodyparser = require('body-parser')
app.use(bodyparser.json())

const NoteRouter = require('./Routes/note')
app.use(NoteRouter)

const UserRouter = require('./Routes/user')
app.use(UserRouter)

app.listen(3000,'0.0.0.0', () => {
    console.log(`SERVER STARTED LISTENING ON PORT 3000`)
})