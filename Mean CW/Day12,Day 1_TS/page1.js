//console.log("How you doing")
// How to declare variables in TS ----------->
var num = 100;
var firstName = 'Malhar';
var lastName = 'D';
var canVote = true;
var address = { country: 'India', state: 'Maharashtra', city: 'Akola', District: 'Akola' };
var description = 'resume';
//console.log(num,firstName,lastName,canVote,address,description)
//------------------------------------------------------------
//-------------------------Functions--------------------------
//------------------------------------------------------------
function add(p1, p2) {
    var result = p1 + p2;
    console.log(result);
}
//add(100,200)
//------------------------------------------------------------
//-------------------------Collections--------------------------
//------------------------------------------------------------
function function1() {
    // array of numbers
    var numbers = [10, 20, 30, 40, 50];
    numbers.push(60);
    numbers.pop();
    //    numbers.push('malhar')
    console.log(numbers);
}
var str = [20, 'Malhar', 30, true];
//console.log(str)
//------------------------------------------------------------
//-------------------------CLASS--------------------------
//------------------------------------------------------------
var cars = /** @class */ (function () {
    function cars() {
    }
    cars.prototype.printInfo = function () {
        console.log(this.Brand);
        console.log(this.Price);
        console.log(this.Model);
        console.log(this.Additionals);
    };
    return cars;
}());
var c1 = new cars();
c1.Brand = 'Maruti Suzuki';
c1.Price = 402000;
c1.Model = 'VXI';
c1.Additionals = {
    SeatWarmers: 'No', Dual_Battery: 'No', Injection_Type: 'CRDI', Fuel: 'Petrol'
};
c1.printInfo();
