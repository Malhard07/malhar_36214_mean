//console.log("How you doing")

// How to declare variables in TS ----------->

const num: number = 100;
const firstName: string = 'Malhar';
const lastName: string = 'D';
const canVote: boolean = true;
const address: object = {country:'India', state:'Maharashtra', city:'Akola', District:'Akola'};
const description: any = 'resume';

//console.log(num,firstName,lastName,canVote,address,description)


//------------------------------------------------------------
//-------------------------Functions--------------------------
//------------------------------------------------------------

function add(p1: number, p2: number){
    const result = p1+p2;
    console.log(result)
}

//add(100,200)

//------------------------------------------------------------
//-------------------------Collections--------------------------
//------------------------------------------------------------

function function1() {
    // array of numbers
    const numbers = [10, 20, 30, 40, 50]
  
    numbers.push(60)
    numbers.pop()
//    numbers.push('malhar')
    console.log(numbers)

}

const str = [20, 'Malhar', 30, true]
//console.log(str)


//------------------------------------------------------------
//-------------------------CLASS--------------------------
//------------------------------------------------------------

class cars 
{
    Brand: string;
    Price: number;
    Model: string;
    Additionals: object;

    printInfo()
    {
        console.log(this.Brand)
        console.log(this.Price)
        console.log(this.Model)
        console.log(this.Additionals)
    }
}

const c1 = new cars()

c1.Brand = 'Maruti Suzuki';
c1.Price = 402000;
c1.Model = 'VXI';
c1.Additionals = {
    SeatWarmers:'No', Dual_Battery:'No', Injection_Type: 'CRDI', Fuel:'Petrol'
}

c1.printInfo()
  