import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []
  prodlength = 0

  constructor( private router:Router,
    private productService:ProductService) { }

  ngOnInit(): void {
    this.loadproducts()
  }

  loadproducts()
  {
    this.productService
        .prodload()
        .subscribe(response => {
          if(response['status'] == 'success')
          {
            this.products = response['data']
            this.prodlength = this.products.length
        
            sessionStorage.setItem('product', JSON.stringify(this.prodlength))
          }
          else{
            console.log(response['error'])
          }
        })
  }

  toggleproduct(product)
  {
    this.productService.prodtoggle(product)
    .subscribe(response => {
      if(response['status'] == 'success')
      {
        this.loadproducts()
      }
      else{
        console.log(response['error'])
      }
    })
  }

  editproduct(product)
  {
    this.router.navigate(['/product-add'], {queryParams: {id: product['id']}})
  }

}
