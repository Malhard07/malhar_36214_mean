import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivate } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AdminService implements CanActivate {

  url = "http://localhost:3000/admin"
  

  constructor(private router: Router,
    private httpClient: HttpClient) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage['token']) 
    {
      return true
    }

    this.router.navigate(['/login'])
    return false 
  }

  adminlogin(email: string, password: string)
  {
   const body ={
      email: email,
      password: password
    }

    return this.httpClient.post(this.url + '/signin',body )
  }
}
