import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = '' 
  password = ''

  constructor(private router:Router,
    private adminService:AdminService ) { }

  ngOnInit(): void {
  }


  onlogin() {
    this.adminService
      .adminlogin(this.email, this.password)
      .subscribe(response => {
        if (response['status'] == 'success') {
          const data = response['data']
          alert('Welcome to the application')
          this.router.navigate(['/dashboard'])
          // cache the user info
          sessionStorage['token'] = data['token']
          sessionStorage['firstName'] = data['firstName']
          sessionStorage['lastName'] = data['lastName']

        } else {
          alert('invalid email or password')
        }
      })
  }

}