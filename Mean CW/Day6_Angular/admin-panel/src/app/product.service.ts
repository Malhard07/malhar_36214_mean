import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }
  
  url = "http://localhost:3000/"
  
  prodload()
  {
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    return this.httpClient.get(this.url + 'product', httpOptions)

  }

  prodtoggle(product)
  {
   
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };

    const body ={
      isActive: product['isActive'] == 1 ? 0 : 1
    }

    return this.httpClient.put(this.url + 'product/' + 'update-state/' + product['id'] + '/' + body['isActive'], body, httpOptions)
  }

  getProductDetails(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.get(this.url + 'product/' +  "/details/" + id, httpOptions)
 }

  updateProduct(id, title: string, description: string, price: number) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };

   const body = {
     title: title,
     description: description,
     price: price
   }
   
   return this.httpClient.put(this.url + "/" + id, body, httpOptions)
 }
}
