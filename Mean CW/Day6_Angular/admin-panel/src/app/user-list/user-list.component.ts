import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users = []
  //users1 = []
  dummy = 0
  // noofusers()
  // { let count = 0;
  //   for(let i=0;i<this.users.length;i++)
  //   {
  //     count++;
  //   }
  //   console.log(count)
  // }

  constructor(private userService:UserService) 
  {}

  

  ngOnInit(): void {
    this.getusers()
  }

  getusers()
  {
    this.userService
    .adminuser()
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.users = response['data']
       
        this.dummy = this.users.length
        sessionStorage.setItem('count', JSON.stringify(this.dummy)) 
        } else {
        console.log(response['error'])
      }
    })
  }
  

  toggleuser(user)
  {
    this.userService
    .toggle(user)
    .subscribe(response => {
      console.log(response)
      if (response['status'] == 'success') {
        this.getusers()
        //alert('YES success')
      } else {
        console.log(response['error'])
        //alert('NOOOO success')

      }
    })
  }
}
