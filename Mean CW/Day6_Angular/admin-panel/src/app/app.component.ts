import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'admin-panel';

  constructor(
    private router: Router) {
  }

  Logout()
  {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('firstName')

    this.router.navigate(['/login'])
  }

}
