import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderListComponent } from './order-list/order-list.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductListComponent } from './product-list/product-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminService } from './admin.service';

const routes: Routes = [
  {path:'order-list', component:OrderListComponent, canActivate: [AdminService]},
  {path:'product-add', component:ProductAddComponent, canActivate: [AdminService]},
  {path:'product-list', component:ProductListComponent, canActivate: [AdminService]},
  {path:'user-list', component:UserListComponent, canActivate: [AdminService]},
  {path:'signup', component:SignupComponent},
  {path:'login', component:LoginComponent},
  {path:'dashboard', component:DashboardComponent, canActivate: [AdminService]},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
