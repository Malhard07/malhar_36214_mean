import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

email = ''
password = ''

  constructor(private toastr:ToastrService,
    private router:Router,
    private authService:AuthService) { }

  ngOnInit(): void {
  }

  onLogin()
  {
    this.authService.login(this.email, this.password)
    .subscribe(response => {
      console.log(response)
      if(response['status'] == 'success')
      {
        this.toastr.success('Welcome to mystore')
        this.router.navigate(['/home'])

      }
      else
      {
        console.log(response['error'])
      }
    })
  }

}
