import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate  {

  url = "http://localhost:4000/user"

  constructor(private httpClient:HttpClient) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean
  {
    return true
  }




  login(email: string, password: string)
  {
    const body = {
      email:email,
      password:password
    }

    return this.httpClient.post(this.url + '/signin', body)

  }

}
