import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path:'', redirectTo: '/home',pathMatch:'full'},
  {path:'home', component:HomeComponent, canActivate:[AuthService]},
  {path: 'auth',loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
