import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Email = ''
  Password = ''

  constructor(private router: Router,
    private studentService:StudentService) { }

  ngOnInit(): void {
  }

  login()
  {
    this.studentService.onlogin(this.Email, this.Password)
    .subscribe(response => {
      if(response['status'] == 'success')
      {
        const data = response['data']

        sessionStorage['token'] = data['token']
        sessionStorage['FirstName'] = data['FirstName']
        sessionStorage['LastName'] = data['LastName']
        this.router.navigate(['/home'])
        alert('login successful')
      }
      else{
        console.log(response['error'])
      }
    })
  }

}
