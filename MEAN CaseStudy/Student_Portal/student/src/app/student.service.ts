import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StudentService implements CanActivate {

  url = "http://localhost:3000/student"

  constructor(private router:Router,
    private httpClient: HttpClient) { }

//---------------------------------------------------------------------------------------
  onlogin(Email: string, Password: string)
  {
    const body = {
      Email:Email,
      Password:Password
    }
    console.log(Email)
    console.log(Password)

    return this.httpClient.post(this.url + '/signin', body)
  }
//---------------------------------------------------------------------------------------
  onsignup(FirstName: string, LastName: string, Address: string, 
    Contact: number, Email: string, Password: string, ClassroomId: number)
  {
    const body = {
      FirstName:FirstName,
      LastName: LastName,
      Address:Address, 
      Contact:Contact,
      Email:Email,
      Password:Password,
      ClassroomId:ClassroomId
    }

    return this.httpClient.post(this.url + '/signup', body)
  }
//---------------------------------------------------------------------------------------

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (sessionStorage['token']) {
     
      return true
    }

    // force user to login
    this.router.navigate(['/login'])

    return false 
  }
//---------------------------------------------------------------------------------------

}
