import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomComponent } from './classroom/classroom.component';
import { FacultyComponent } from './faculty/faculty.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { StudentService } from './student.service';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path:'home', component:HomeComponent, canActivate:[StudentService]},
  {path:'classroom', component:ClassroomComponent, canActivate:[StudentService]},
  {path:'faculty', component:FacultyComponent, canActivate:[StudentService]},
  { path: 'login', component: LoginComponent },
  {path:'signup', component:SignupComponent}
 // {path:'', component:},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
