import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  FirstName = ''
  LastName = ''
  Address = ''
  Contact = 0
  Email = ''
  Password = ''
  ClassroomId = 0
  
  constructor(private studentService:StudentService) { }

  ngOnInit(): void {
  }
  signup()
  {
    this.studentService.onsignup(this.FirstName, this.LastName, this.Address, this.Contact,
       this.Email, this.Password, this.ClassroomId)
       .subscribe(response => {
         if(response['status'] == 'success')
         {
           alert('Successfully Registered')
         }
         else{
           console.log(response['error'])
         }
       })
  }

}
