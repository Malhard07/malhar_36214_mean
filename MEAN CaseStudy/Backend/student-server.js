const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
const cors = require('cors')

const studentRouter = require('./student/routes/student')

const app = express()
app.use(cors())
app.use(bodyParser.json())

function getUserId(request, response, next) {

  if (request.url == '/student/signin' 
      || request.url == '/student/signup') 
 {
    next()
  } 
 else {

    try {
      const token = request.headers['token']
      const data = jwt.verify(token, config.secret)

      // add a new key named userId with logged in user's id
      request.studentId = data['Id']

      // go to the actual route
      next()
      
    } catch (ex) {
      response.status(401)
      response.send({status: 'error', error: 'protected api'})
    }
  }
}

app.use(getUserId)

app.use('/student', studentRouter)



// default route
app.get('/', (request, response) => {
  response.send('welcome to my application')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})