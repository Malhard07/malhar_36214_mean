const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------

router.get('/classroom/:sid', (request, response) => {
    const {sid} = request.params

    const statement = `SELECT s.FirstName, s.LastName, c.Name, c.DateTime, c.size
    FROM Student s INNER JOIN Classroom c ON s.ClassroomId = c.Id 
    WHERE s.Id = ${sid} `

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})

router.get('/faculty/:sid', (request, response) => {
    const {sid} = request.params

    const statement = `SELECT f.FirstName, f.LastName, f.Address,f.Contact, f.Email
    FROM ((Student s INNER JOIN Classroom c ON s.ClassroomId = c.Id)
    INNER JOIN Faculty f ON f.Id = c.FacultyId) WHERE s.Id = ${sid};
     `



    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})


// ---------------------------------------
//                  POST
// ---------------------------------------


router.post('/signin', (request, response) => {
    const {email, password} = request.body
    const statement = `SELECT * FROM Student where Email = '${email}' and Password = '${password}'`
    db.query(statement, (error, students) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (students.length == 0) {
          response.send({status: 'error', error: 'student does not exist'})
        } else {
          const student = students[0]
          const token = jwt.sign({Id: student['Id']}, config.secret)
          response.send(utils.createResult(error, {
            FirstName: student['FirstName'],
            LastName: student['LastName'],
            token: token
          }))
        }
      }
    })
  })
  

router.post('/signup', (request, response) => {
    const {FirstName, LastName, Address, Contact, Email, Password, ClassroomId} = request.body

    const statement = `INSERT INTO Student (FirstName, LastName, Address, Contact, 
    Email, Password, ClassroomId)
    VALUES
    ('${FirstName}', '${LastName}', '${Address}', '${Contact}', '${Email}', '${Password}', '${ClassroomId}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



// ---------------------------------------
//                  PUT
// ---------------------------------------

router.post('/update/:Id', (request, response) => {
  const { Id } = request.params
  const {Address, Contact, Email, Password, ClassroomId} = request.body

  const statement = `UPDATE Student SET Address = '${Address}', Contact = '${Contact}', 
  Email = '${Email}', Password = '${Password}', ClassroomId = '${ClassroomId}'
  WHERE Id = ${Id}`

  db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
  })
})



// ---------------------------------------
//                  DELETE
// ---------------------------------------



module.exports = router



