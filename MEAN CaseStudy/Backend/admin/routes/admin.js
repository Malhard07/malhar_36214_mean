const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------

router.get('/allfaculty', (request, response) => {
    
    const statement = `SELECT * FROM Faculty`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/allstudent', (request, response) => {
    
    const statement = `SELECT * FROM Student`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/student/room/:id', (request, response) => {
    const { id } = request.params
    
    const statement = `SELECT * FROM Student WHERE ClassroomId = ${id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
    })
})

router.get('/student/faculty/:id', (request, response) => {
    const { id } = request.params
    
    const statement = `SELECT s.FirstName, s.LastName, s.Address, s.Contact, s.Email
    FROM Student s INNER JOIN Classroom c ON s.ClassroomId = c.Id   
    WHERE c.FacultyId = ${id}`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
    })
})


// ---------------------------------------
//                  POST
// ---------------------------------------


router.post('/signin', (request, response) => {
    const {Email, Password} = request.body
    const statement = `SELECT * FROM Admin where Email = '${Email}' and Password = '${Password}'`
    db.query(statement, (error, admins) => {
        console.log(statement)
      if (error) {
        response.send({status: 'error', error: error})
      } else {
          console.log(admins)
        if (admins.length == 0) {
          response.send({status: 'error', error: 'admin does not exist'})
        } else {
          const admin = admins[0]
          const token = jwt.sign({Id: admin['Id']}, config.secret)
          response.send(utils.createResult(error, {
            FirstName: admin['FirstName'],
            LastName: admin['LastName'],
            token: token
          }))
        }
      }
    })
  })
  

router.post('/signup', (request, response) => {
    const {FirstName, LastName, Address, Contact, Email, Password} = request.body

    const statement = `INSERT INTO Admin (FirstName, LastName, Address, Contact, 
    Email, Password)
    VALUES
    ('${FirstName}', '${LastName}', '${Address}', '${Contact}', '${Email}', '${Password}')`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



// ---------------------------------------
//                  PUT
// ---------------------------------------




// ---------------------------------------
//                  DELETE
// ---------------------------------------



module.exports = router



