const express = require('express')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
const cors = require('cors')

const adminRouter = require('./admin/routes/admin')

const app = express()
app.use(cors())
app.use(bodyParser.json())

function getUserId(request, response, next) {

  if (request.url == '/admin/signin' 
      || request.url == '/admin/signup') 
 {
    next()
  } 
 else {

    try {
      const token = request.headers['token']
      const data = jwt.verify(token, config.secret)

      request.adminId = data['Id']

      // go to the actual route
      next()
      
    } catch (ex) {
      response.status(401)
      response.send({status: 'error', error: 'protected api'})
    }
  }
}

app.use(getUserId)

app.use('/admin', adminRouter)



// default route
app.get('/', (request, response) => {
  response.send('welcome to ADMIN-SIDE application')
})

app.listen(4000, '0.0.0.0', () => {
  console.log('server started on port 4000')
})