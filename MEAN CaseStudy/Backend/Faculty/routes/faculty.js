const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const jwt = require('jsonwebtoken')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------

// router.get('/classroom/:sid', (request, response) => {
//     const {sid} = request.params

//     const statement = `SELECT s.FirstName, s.LastName, c.Name, c.DateTime, c.size
//     FROM Student s INNER JOIN Classroom c ON s.ClassroomId = c.Id 
//     WHERE s.Id = ${sid} `

//     db.query(statement, (error, data) => {
//         response.send(utils.createResult(error, data))
//     })
// })

// router.get('/faculty/:sid', (request, response) => {
//     const {sid} = request.params

//     const statement = `SELECT f.FirstName, f.LastName, f.Address,f.Contact, f.Email
//     FROM ((Student s INNER JOIN Classroom c ON s.ClassroomId = c.Id)
//     INNER JOIN Faculty f ON f.Id = c.FacultyId) WHERE s.Id = ${sid};
//      `



//     db.query(statement, (error, data) => {
//         response.send(utils.createResult(error, data))
//     })
// })


// ---------------------------------------
//                  POST
// ---------------------------------------


router.post('/signin', (request, response) => {
    const {Email, Password} = request.body
    const statement = `SELECT * FROM Faculty where Email = '${Email}' and Password = '${Password}'`
    db.query(statement, (error, faculties) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (faculties.length == 0) {
          response.send({status: 'error', error: 'faculty does not exist'})
        } else {
          const faculty = faculties[0]
          const token = jwt.sign({Id: faculty['Id']}, config.secret)
          response.send(utils.createResult(error, {
            FirstName: faculty['FirstName'],
            LastName: faculty['LastName'],
            token: token
          }))
        }
      }
    })
  })
  

router.post('/signup', (request, response) => {
    const {FirstName, LastName, Address, Contact, Email, Password, Age, Experience} = request.body

    const statement = `INSERT INTO Faculty (FirstName, LastName, Address, Contact, 
    Email, Password, Age, Experience)
    VALUES
    ('${FirstName}', '${LastName}', '${Address}', '${Contact}', '${Email}', '${Password}', ${Age}, ${Experience})`

    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})



// ---------------------------------------
//                  PUT
// ---------------------------------------

router.post('/update/:Id', (request, response) => {
  const { Id } = request.params
  const {Address, Contact, Email, Password, Age, Experience} = request.body

  const statement = `UPDATE Student SET Address = '${Address}', Contact = '${Contact}', 
  Email = '${Email}', Password = '${Password}', Age = ${Age}, Experience = ${Experience}
  WHERE Id = ${Id}`

  db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
  })
})



// ---------------------------------------
//                  DELETE
// ---------------------------------------



module.exports = router



